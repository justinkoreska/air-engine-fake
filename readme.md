# Air Engine Fake

Fake it 'til you make it!

A proxy to Air Engine with rules to manipulate responses for testing purposes and a web app for managing the rules.

## Getting Started

* `> npm install`
* `> npm start`
* `> open http://localhost:5000`
* Change `airengine.api.url` to `http://localhost:5000/api` in `common-backend/src/main/resources/backend-application.properties` to use Air Engine Fake with your build.
