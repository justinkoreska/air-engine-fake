import path from "path";
import express from "express";
import httpProxy from "http-proxy";
import BufferHelper from "bufferhelper";

import RulesService from "../services/rules-service";

const rulesFile = process.env.AIRENGINEFAKE_RULESFILE || path.resolve(__dirname, "..", "rules.json");
const rulesService = new RulesService(rulesFile);

const target = process.env.AIRENGINEFAKE_TARGET || "http://fnxml.flightnetwork.com/api";
const proxy = httpProxy.createProxyServer({ target });

const proxyHandler = (proxyResponse, request, response) => {

  const rules = rulesService.findRules({
    enabled: true,
    proxy: true,
    path: request.path,
    test: request.cookies.fake,
  });

  delete proxyResponse.headers["content-length"];

  const buffer = new BufferHelper();
  const _write = response.write.bind(response);
  const _end = response.end.bind(response);

  response.write = data => {
    buffer.concat(data);
  };
  response.end = () => {
    let data = buffer.toBuffer();

    if (rules.length) {
      let body = JSON.parse(buffer.toString());

      for (const rule of rules) {
        if (rule.transformer)
          body = eval(rule.transformer)(rule, body);
        if (rule.status)
          response.status(rule.status);
      }

      data = new Buffer(JSON.stringify(body));
    }

    _write(data);
    _end();
  };
};

const requestHandler = (request, response) => {

  console.log(request.path);

  const rules = rulesService.findRules({
    enabled: true,
    proxy: false,
    path: request.path,
    test: request.cookies.fake,
  });

  let body = null;

  for (const rule of rules) {
    if (rule.transformer)
      body = eval(rule.transformer)(rule, body);
    if (rule.status)
      response.status(rule.status);
  }

  if (body) {
    response.send(body);
    return;
  }

  delete request.headers["accept-encoding"];

  proxy.web(request, response);
};

proxy.on("proxyRes", proxyHandler);

export default requestHandler;
