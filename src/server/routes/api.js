import express from "express";
import rulesHandler from "../middleware/rules-handler";

const router = express.Router();

router.use(rulesHandler);

export { router };
