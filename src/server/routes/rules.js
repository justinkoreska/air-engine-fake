import path from "path";
import express from "express";

import RulesService from "../services/rules-service";

const router = express.Router();

const rulesFile = process.env.AIRENGINEFAKE_RULESFILE || path.resolve(__dirname, "..", "rules.json");
const rulesService = new RulesService(rulesFile);

router.get("*", (request, response) => {
  const rules = rulesService.findRules();
  response.send(rules);
});

router.post("*", (request, response) => {
  const rule = rulesService.setRule(request.body);
  response.send(rule);
});

router.delete("/:id", (request, response) => {
  rulesService.deleteRule({ id: +request.params.id });
  response.send();
});

export { router };
