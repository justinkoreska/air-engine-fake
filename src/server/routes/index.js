
import { router as assets } from "./assets";
import { router as rules } from "./rules";
import { router as page } from "./page";
import { router as api } from "./api";

const routes = {
  "/assets": assets,
  "/rules": rules,
  "/api": api,
  "/": page,
};

export default routes;
