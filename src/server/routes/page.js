import fs from "fs";
import path from "path";
import express from "express";
import handlebars from "handlebars";

const router = express.Router();

router.get("*", (request, response) => {
  const template = handlebars.compile(
    fs.readFileSync(
      path.join(__dirname, "..", "client", "index.html"),
      "utf8"
    )
  );
  response.send(
    template({ title: "Air Engine Fake" })
  );
});

export { router };
