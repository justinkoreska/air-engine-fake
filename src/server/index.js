import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import routes from "./routes";

const server = express();

server.use(bodyParser.json());
server.use(cookieParser());

Object.keys(routes).forEach(route => {
  server.use(route, routes[route]);
});

const port = process.env.AIRENGINEFAKE_PORT || 5000;

console.log(`Starting server on ${port}`);
server.listen(port);
