import jsonfile from "jsonfile";

const normalizePath = path =>
  path.replace(/(^\/|\/json$)/g, "");

class RulesService {

  constructor(rulesFile) {
    this._rulesFile = rulesFile;
    //TODO check and init file
  }

  readRules() {
    return jsonfile.readFileSync(this._rulesFile);
  }

  writeRules(rules) {
    return jsonfile.writeFileSync(this._rulesFile, rules);
  }

  getRule(id) {
    return this.findRules({ id });
  }

  setRule(rule) {
    if (!rule || !rule.name || !rule.path)
      throw new Error("missing rule name and path");
    rule.path = normalizePath(rule.path);
    const rules = this.readRules();
    if (!rule.id) {
      const maxId = rules.reduce((max, rule) => rule.id > max ? rule.id : max, 0);
      rule.id = maxId + 1;
      rules.push(rule);
    } else {
      rules.forEach(r =>
        rule.id === r.id && Object.assign(r, rule)
      );
    }
    this.writeRules(rules);
    return rule;
  }

  deleteRule(rule) {
    const rules = this.readRules()
      .filter(r => r.id != rule.id);
    this.writeRules(rules);
    return rules;
  }

  findRules(filters) {
    const rules = this.readRules();
    return "object" != typeof filters ? rules :
      rules.filter(rule =>
        Object
          .keys(filters)
          .every(filter => {
            switch(filter) {
              case "path":
                return rule.path === normalizePath(filters.path);
              case "test":
                return !filters.test && !rule.test || rule.test && rule.test.match(filters.test || 0);
              default:
                return "undefined" == typeof rule[filter] || rule[filter] === filters[filter];
            }
          })
    );
  }

}

export default RulesService;
