import { expect } from "chai";
import jsonfile from "jsonfile";

import RulesService from "./rules-service";

describe("RulesService", function() {

  const rulesFile = "./rules.json";
  jsonfile.writeFileSync(rulesFile, [
    { enabled: true, path: "search", test: "" },
    { enabled: true, path: "review", test: "" },
    { enabled: false, path: "review", test: "fakeit" },
    { enabled: false, path: "search" },
  ]);

  const rulesService = new RulesService(rulesFile);

  it("should find enabled rules", () => {
    const rules = rulesService.findRules({ enabled: true });
    expect(rules.length).to.equal(2);
  });

  it("should find rules that match test", () => {
    const rules = rulesService.findRules({ test: "fakeit" });
    expect(rules.length).to.equal(1);
  });

  it("should find rules that have no test when no match defined", () => {
    const rules = rulesService.findRules({ test: undefined });
    expect(rules.length).to.equal(3);
  });

});
