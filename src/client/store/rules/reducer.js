import types from "./types";

const defaultState = {
  rules: [],
  loading: false,
  saving: false,
  selected: null,
};

const rulesLoading = (state, action) =>
  state.loading == action.payload
    ? state
    : Object.assign({}, state, { loading: action.payload });

const rulesLoaded = (state, action) =>
  Object.assign({}, state, { rules: action.payload, selected: null });

const ruleSaving = (state, action) =>
  state.saving == action.payload
    ? state
    : Object.assign({}, state, { saving: action.payload });

const ruleSelected = (state, action) =>
  state.selected === action.payload
    ? state
    : Object.assign({}, state, { selected: action.payload });

export default (state, action) => {
  switch(action.type) {
    case types.RULES_LOADING: return rulesLoading(state, action);
    case types.RULES_LOADED: return rulesLoaded(state, action);
    case types.RULE_SAVING: return ruleSaving(state, action);
    case types.RULE_SELECTED: return ruleSelected(state, action);
    default: return state || defaultState;
  }
};
