
const rules = ({ rules: { rules } }) => rules;

const loading = ({ rules: { loading } }) => loading;

const saving = ({ rules: { saving } }) => saving;

const selected = ({ rules: { selected } }) => selected;

export default {
  rules,
  loading,
  saving,
  selected,
};
