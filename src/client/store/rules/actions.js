
import types from "./types";

const rulesLoading = loading => ({
  type: types.RULES_LOADING,
  payload: loading,
});

const rulesLoaded = rules => ({
  type: types.RULES_LOADED,
  payload: rules,
});

const loadRules = () => (dispatch, getState, services) => {
  dispatch(rulesLoading(true));
  return services.rulesService
    .getRules()
    .then(rules => {
      dispatch(rulesLoaded(rules));
      dispatch(rulesLoading(false));
    });
};

const ruleSaving = saving => ({
  type: types.RULE_SAVING,
  payload: saving,
});

const saveRule = rule => (dispatch, getState, services) => {
  dispatch(ruleSaving(true));
  return services.rulesService
    .setRule(rule)
    .then(() => {
      dispatch(ruleSaving(false));
      dispatch(loadRules());
    });
};

const ruleSelected = rule => ({
  type: types.RULE_SELECTED,
  payload: rule,
});

const selectRule = rule =>
  ruleSelected(rule);

const deleteRule = rule => (dispatch, getState, services) => {
  dispatch(ruleSaving(true));
  return services.rulesService
    .deleteRule(rule)
    .then(() => {
      dispatch(ruleSaving(false));
      dispatch(loadRules());
    });
};

export default {
  rulesLoading,
  rulesLoaded,
  loadRules,
  ruleSaving,
  saveRule,
  selectRule,
  deleteRule,
};
