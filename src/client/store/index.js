import { routerReducer as router } from "react-router-redux";

import { reducer as user } from "./user";
import { reducer as config } from "./config";
import { reducer as rules } from "./rules";

export default {
  router,
  user,
  config,
  rules,
};
