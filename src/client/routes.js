import React from "react";
import {
  Route,
  Switch,
} from "react-router-dom";

import HomePage from "./components/home-page";
import ErrorPage from "components/error-page";

export default (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route component={ErrorPage} />
  </Switch>
);
