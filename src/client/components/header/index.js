import React from "react";
import { Link } from "react-router-dom";

import "./scss/index.scss";

const HeaderView = () => (
  <header className="Header">
    <nav className="navbar">
      <div className="navbar-brand">
        <Link to="/" className="navbar-item">
          <img className="Header-logo" src="" alt="FlightNetwork" />
          <h1 className="is-size-4">
          </h1>
        </Link>
        <div className={`navbar-burger burger`} onClick={console.log}>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div className="navbar-menu">
        <div className="navbar-end">
        </div>
      </div>
    </nav>
  </header>
);

export default HeaderView;
