import React from "react";
import moment from "moment";

import "./scss/index.scss";

class DateInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      calendar: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) {
      const selected = moment(nextProps.value);
      this.setState({
        selected,
        value: selected.format("ddd MMM D"),
      });
    }
  }

  generateCalendar() {
    const calendar = [];
    const start = moment().startOf("month");

    for (let n = 0; n < start.weekday(); n++)
      calendar.push(null);
    for (let n = 0; n < 365; n++)
      calendar.push(start.clone().add(n, "days"));

    this.setState({
      calendar
    });
  }

  onChange(event) {
    this.setState({
      value: "",
    });
  }

  onFocus(event) {
    this.setState({
      isSelecting: true,
      value: "",
    });
  }

  onBlur(event) {
    setTimeout(() => {
      const value = this.state.selected
        ? this.state.selected.format("ddd MMM D")
        : "";
      this.setState({
        value,
        isSelecting: false,
      });
    }, 100);
  }

  onEnter(day) {
    this.setState({
      hovered: day,
    });
  }

  onOut(day) {
    this.setState({
      hovered: day,
    });
  }

  onClick(day) {
    this.setState({
      selected: day,
    });
    this.props.onSelect(day);
  }

  className(day) {
    let className = "";
    const { hovered, selected } = this.state;
    const { anchor } = this.props;

    if (hovered === day)
      className += " active";
    if (selected === day || selected && selected.isSame(day))
      className += " selected";
    if (anchor && anchor.isSame(day))
      className += " anchor";

    if (anchor && hovered && (
        day.isBetween(anchor, hovered) ||
        day.isBetween(hovered, anchor)
        ))
      className += " between";

    if (anchor && selected && (
        day.isBetween(anchor, selected) ||
        day.isBetween(selected, anchor)
        ))
      className += " between";

    return className;
  }

  componentDidMount() {
    this.generateCalendar();
  }

  render() {
    let emptyIndex = 0;
    return (
      <span className="DateInput">
        <input
          type="text"
          value={this.state.value}
          onChange={this.onChange.bind(this)}
          onFocus={this.onFocus.bind(this)}
          onBlur={this.onBlur.bind(this)}
          placeholder={this.props.placeholder}
        />
        {this.state.isSelecting &&
          <ul className="DateInput-calendar flex seven">
            {this.state.calendar.map(day =>
              day ?
              <li
                id={day.format("YYYY-MM-DD")}
                key={day.format("YYYY-MM-DD")}
                onMouseEnter={e => this.onEnter.bind(this)(day)}
                onMouseOut={e => this.onOut.bind(this)(day)}
                onClick={e => this.onClick.bind(this)(day)}
                className={this.className.bind(this)(day)}
                >
                <div className="DateInput-month">{day.format("MMM")}</div>
                <div className="DateInput-date">{day.format("D")}</div>
              </li>
              :
              <li className="empty" key={emptyIndex++}></li>
            )}
          </ul>
        }
      </span>
    );
  }
}

export default DateInput;
