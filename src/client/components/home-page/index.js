import React from "react";

import Header from "components/header";
import RuleList from "components/rule-list";
import RuleForm from "components/rule-form";

import "./index.scss";

const HomePage = () =>
  <div className="HomePage">
    <Header/>
    <section className="HomePage-content">
      <div className="container">
        <h1 className="title">Rules</h1>
        <div className="columns">
          <div className="column">
            <RuleList/>
          </div>
          <div className="column">
            <RuleForm/>
          </div>
        </div>
      </div>
    </section>
  </div>;

export default HomePage;
