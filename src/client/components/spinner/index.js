import React from "react";
import Spin from "spin";

class Spinner extends React.Component {

  static defaultConfig = {
    lines: 10,
    radius: 5,
    width: 5,
    length: 5,
    width: 5,
  };

  getConfig() {
    return Object.assign({}, Spinner.defaultConfig, this.props.config);
  }

  componentDidMount() {
    this._spinner = new Spin(this.getConfig());
    if (this.props.spinning)
      this._spinner.spin(this.refs.container);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.spinning && !this.props.spinning) {
      this._spinner.spin(this.refs.container);
    } else if (!newProps.spinning && this.props.spinning) {
      this._spinner.stop();
    }
  }

  componentWillUnmount() {
    this._spinner.stop();
  }

  render() {
    return (
      <span className="Spinner" ref="container" />
    );
  }
}

export default Spinner;
