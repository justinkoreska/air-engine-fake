import React from "react";

import RuleFormView from "./view";

class RuleFormController extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      form: null,
      errors: {},
      advancedOpen: false,
    };
  }

  componentWillReceiveProps(props) {
    const form = props.rule
      ? {
          ...props.rule,
          settingsString: JSON.stringify(props.rule.settings),
        }
      : null;
    this.setState({
      form,
      errors: {},
      advancedOpen: false,
    });
  }

  update(field, value) {
    const form = this.state.form;
    const errors = this.state.errors;
    if ("transformer" == field)
      errors[field] = this.validateCode(value);
    if ("settingsString" == field)
      errors[field] = this.validateJSON(value);
    if ("settings" == field)
      form.settingsString = JSON.stringify(value);
    this.setState({
      form: {
        ...form,
        [field]: value,
      },
      errors,
    });
  }

  validateCode(code) {
    let error = null;
    try {
      eval(code);
    } catch(e) {
      error = e.message;
    }
    return error;
  }

  validateJSON(json) {
    let error = null;
    try {
      JSON.parse(json);
    } catch(e) {
      error = e.message;
    }
    return error;
  }

  save(e) {
    e.preventDefault();
    this.props.saveRule(
      this.mapForm(this.state.form)
    );
  }

  select(rule) {
    return e => {
      e.preventDefault();
      this.props.selectRule(rule);
    };
  }

  remove(e) {
    e.preventDefault();
    this.props.deleteRule(this.props.rule);
  }

  mapForm(form) {
    const error = this.validateJSON(form.settingsString);
    const settings = !error ? JSON.parse(form.settingsString) : {};
    return {
      id: form.id,
      path: form.path,
      test: form.test,
      enabled: form.enabled,
      proxy: form.proxy,
      name: form.name,
      text: form.text,
      transformer: form.transformer,
      status: form.status,
      settings,
    };
  }

  toggleAdvanced(e) {
    e.preventDefault();
    this.setState({
      advancedOpen: !this.state.advancedOpen,
    });
  }

  render() {
    return RuleFormView({
      ...this.props,
      ...this.state,
      update: this.update.bind(this),
      save: this.save.bind(this),
      select: this.select.bind(this),
      remove: this.remove.bind(this),
      toggleAdvanced: this.toggleAdvanced.bind(this),
    });
  }
}

export default RuleFormController;
