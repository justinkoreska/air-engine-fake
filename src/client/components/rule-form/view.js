import React from "react";

import "./index.scss";

const RuleFormView = ({
  form,
  update,
  errors,
  save,
  saving,
  select,
  remove,
  advancedOpen,
  toggleAdvanced,
}) =>
  <section className="RuleForm box">

    {!form &&
      <div className="has-text-centered">
        <h3 className="title has-text-light">
          Select a rule or
        </h3>
        <button className="button is-large" onClick={select({})}>
          Create a rule
        </button>
      </div>
    }

    {form &&
      <div>
        <h3 className="title is-5">
          {form.name}
        </h3>
        <h4 className="subtitle is-6">
          {form.text}
        </h4>
        <form onSubmit={save}>

          <div className="field">
            <div className="control">
              <label className="checkbox">
                <input
                  type="checkbox"
                  checked={form.enabled}
                  onChange={e => update("enabled", e.target.checked)}
                />
                <span> enabled</span>
              </label>
            </div>
          </div>

          {form.settings &&
            <p>
              {Object.keys(form.settings).map(key =>
                <input
                  className="input"
                  key={key}
                  type={form.settings[key].type}
                  defaultValue={form.settings[key].value}
                  placeholder={form.settings[key].name}
                  onChange={e =>
                    update("settings", {
                      ...form.settings,
                      [key]: {
                        ...form.settings[key],
                        value: e.target.value,
                      },
                    })
                  }
                />
              )}
            </p>
          }

          <hr/>

          <button className="button is-white" onClick={toggleAdvanced}>
            <span className="icon">
              <i className={`fa fa-arrow-${advancedOpen ? "down" : "right"}`}></i>
            </span>
            <span>
              advanced settings
            </span>
          </button>

          {advancedOpen &&
            <div>
              <div className="field">
                <label className="label">name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    placeholder=""
                    value={form.name}
                    onChange={e => update("name", e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">description</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    placeholder=""
                    value={form.text}
                    onChange={e => update("text", e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">path</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    placeholder=""
                    value={form.path}
                    onChange={e => update("path", e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">
                  test
                  <span className="has-text-weight-light"> (regex on cookie.fake)</span>
                </label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    placeholder=""
                    value={form.test}
                    onChange={e => update("test", e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">proxy</label>
                <div className="control">
                  <label className="checkbox">
                    <input
                      type="checkbox"
                      checked={form.proxy}
                      onChange={e => update("proxy", e.target.checked)}
                    />
                  </label>
                </div>
              </div>
              <div className="field">
                <label className="label">transformer</label>
                <div className="control">
                  <textarea
                    className={`textarea is-code ${errors.transformer ? "is-danger" : ""}`}
                    placeholder="e.g. (rule, body) => body"
                    value={form.transformer}
                    onChange={e => update("transformer", e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">settings</label>
                <div className="control">
                  <textarea
                    className={`textarea is-code ${errors.settingsString ? "is-danger" : ""}`}
                    placeholder='e.g. [{ type: "text", name: "count", value: 1 }]'
                    value={form.settingsString}
                    onChange={e => update("settingsString", e.target.value)}
                  />
                </div>
              </div>
            </div>
          }

          <hr/>

          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <button className="button is-light" onClick={select(null)}>Cancel</button>
            </div>
            <div className="control">
              <button className="button is-danger" onClick={remove}>Delete</button>
            </div>
            <div className="control">
              <button type="submit" className="button is-primary" disabled={saving}>Submit</button>
            </div>
          </div>

        </form>
      </div>
    }

  </section>;

export default RuleFormView;
