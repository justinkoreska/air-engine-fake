import { connect } from "react-redux";

import {
  actions as rulesActions,
  selectors as rulesSelectors,
} from "store/rules";

import RuleFormController from "./controller";

const mapState = state => ({
  rule: rulesSelectors.selected(state),
  saving: rulesSelectors.saving(state),
});

const mapDispatch = dispatch => ({
  saveRule: rule => dispatch(rulesActions.saveRule(rule)),
  selectRule: rule => dispatch(rulesActions.selectRule(rule)),
  deleteRule: rule => dispatch(rulesActions.deleteRule(rule)),
});

const RuleForm = connect(mapState, mapDispatch)(RuleFormController);

export default RuleForm;
