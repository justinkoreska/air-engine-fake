import { connect } from "react-redux";

import {
  actions as rulesActions,
  selectors as rulesSelectors,
} from "store/rules";

import RuleListController from "./controller";

const mapState = state => ({
  rules: rulesSelectors.rules(state),
  loading: rulesSelectors.loading(state),
  selected: rulesSelectors.selected(state),
});

const mapDispatch = dispatch => ({
  selectRule: rule => e => dispatch(rulesActions.selectRule(rule)),
});

const RuleList = connect(mapState, mapDispatch)(RuleListController);

export default RuleList;
