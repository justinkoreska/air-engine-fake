import React from "react";

import "./index.scss";

const RuleListView = ({
  rules,
  loading,
  selectRule,
  selected,
  term,
  onFilter,
}) =>
  <section className="RuleList">

    <div>
      <div className="field">
        <div className="control">
          <input
            type="text"
            className="input"
            placeholder="search"
            value={term}
            onChange={onFilter}
          />
        </div>
      </div>
    </div>

    <table className="table is-fullwidth is-striped is-hoverable">
      <thead>
        <tr>
          <th>on</th>
          <th>proxy</th>
          <th>path</th>
          <th>name</th>
          <th>description</th>
        </tr>
      </thead>
      <tbody>
        {rules.map(rule =>
          <tr
            key={rule.id}
            className={selected && selected.id === rule.id ? "is-selected" : ""}
            onClick={selectRule(rule)}
            >
            <td>
              <label className="checkbox">
                <input type="checkbox" checked={rule.enabled} disabled />
              </label>
            </td>
            <td>
              <label className="checkbox">
                <input type="checkbox" checked={rule.proxy} disabled />
              </label>
            </td>
            <td>
              {rule.path}
            </td>
            <td>
              {rule.name}
            </td>
            <td>
              {rule.text}
            </td>
          </tr>
        )}
      </tbody>
    </table>
  </section>;

export default RuleListView;
