import React from "react";

import RuleListView from "./view";

class RuleListController extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      term: "",
      rules: [],
    };
  }

  componentWillReceiveProps(props) {
    this.filter(props.rules, this.state.term);
  }

  onFilter(e) {
    const term = e.target.value;
    this.filter(this.props.rules, term);
  }

  filter(unfiltered, term) {
    const rules = unfiltered.filter(rule =>
      !term ||
      rule.name.indexOf(term) >= 0 ||
      rule.path.indexOf(term) >= 0 ||
      rule.test && rule.test.indexOf(term) >= 0
    );
    this.setState({
      term,
      rules,
    });
  }

  render() {
    return RuleListView({
      ...this.props,
      ...this.state,
      onFilter: this.onFilter.bind(this),
    });
  }
}

export default RuleListController;
