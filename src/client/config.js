
export default {
  apiBase: "/api",
  fnxmlBase: "https://fnxml.flightnetwork.com/api",
  cookie: {
    domain: "localhost",
  },
};
