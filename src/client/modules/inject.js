import PropTypes from "prop-types";

const inject = Component => {
  Component.contextTypes = { services: PropTypes.object };
  return Component;
};

export default inject;
