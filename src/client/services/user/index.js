import { jsonHandler } from "modules/fetch";

class UserService {

  constructor(config, fetch) {
    this.config = config || {};
    this.fetch = fetch;
  }

  signinFake(username, password) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        username === password
          ? resolve({ username })
          : reject({ error: "auth failed" });
      }, 2000);
    });
  }

  signin(username, password) {
    return this.fetch(
      this.config.apiBase + "/auth", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          username,
          password,
        }),
      })
      .then(jsonHandler)
  }

}

export default UserService;
