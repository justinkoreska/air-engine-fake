import { jsonHandler } from "modules/fetch";

class RulesService {

  constructor(config, fetch) {
    this.config = config || {};
    this.fetch = fetch;
  }

  getRules() {
    return fetch("/rules/")
      .then(jsonHandler);
  }

  setRule(rule) {
    return fetch("/rules/", {
      headers: {
        "Content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(rule),
    });
  }

  deleteRule(rule) {
    return fetch("/rules/" + rule.id, {
      method: "DELETE",
    });
  }

}

export default RulesService;
