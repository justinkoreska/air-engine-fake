import Bottle from "bottlejs";

import "whatwg-fetch";

import config from "./config";
import UserService from "services/user";
import RulesService from "services/rules";

const kernel = new Bottle();

kernel.service("config", () => config);
kernel.service("location", () => window.location);
kernel.service("history", () => window.history);
kernel.service("fetch", () => fetch.bind(window));
kernel.service("document", () => document);

kernel.service("userService", UserService, "config", "fetch");
kernel.service("rulesService", RulesService, "config", "fetch");

export default kernel.container;
